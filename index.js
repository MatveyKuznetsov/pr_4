const BASE_URL = 'http://www.omdbapi.com'
const KEY = 'FILMS'

Vue.component('search-component', {
    template: `
        <div class="field">
            <input
                v-model="searchString"
                type="text"
                class="input">
            <input
                type="submit"
                class="searchBtn"
                value="Search"
                @click="search">
        </div>
    `,
    data: function () {
        return {
            searchString: ''
        }
    },
    methods: {
        encodeParams: function (params) { 
            return Object.keys(params).map(key => 
                [key, params[key]].map(encodeURIComponent).join('=')
            ).join('&')
        },
        request: async function (requestURL) {
            const responce = await fetch(requestURL)
            const result = await responce.json()
            if (result.Response === "False") throw result
            return result
        },
        search: async function () {
            if (!this.searchString.trim()) return
            const queryParams = {
                s: this.searchString.trim(),
                apikey: 'd5677312'
            }
            const query = this.encodeParams(queryParams)
            const requestURL = `${BASE_URL}/?${query}`
            
            try {
                const data = await this.request(requestURL)
                this.$emit('search', data.Search)
            } catch(err) {
                console.error(err)
                // if (err.Error) this.error(err.Error)
                // else {
                //     console.error(err)
                //     this.error('Sorry, something wents wrong :(')
                // }
            }
        }
    },
    created: function () {
        window.onkeydown = e => e.keyCode === 13 ? this.search() : null
    }

})

Vue.component('results-component', {
    template: `
        <div class="results">
            <p
                v-if="errorStatus.is"
                class="error">
                {{errorStatus.message}}
            </p>
            <div class="films">
                <film-item
                    v-for="(film, i) in filmsToRender"
                    :key="i"
                    :film="film">
                </film-item>
            </div>
        </div>
    `,
    props: {
        errorStatus: Object,
        films: Array
    },
    data: function () {
        return {
            filmsToRender: []
        }
    },
    watch: {
      films: function (n, o) {
          this.filmsToRender = n
      }  
    },
    created: function () {
        // if (!this.films.length) {
        this.filmsToRender = JSON.parse(localStorage.getItem(KEY) || '[]')
        console.log(this.filmsToRender);
        
        // } else {
        //     this.filmsToRender = this.films
        // }
    },
})

Vue.component('film-item', {
    template: `
        <div class="film">
            <div class="poster" @click="open">
                <img :src="poster" alt>
            </div>
            <div class="info">
                <h2 class="film-name" @click="open">{{title}}</h2>
                <p class="year">{{year}}</p>
                <button class="favourite" @click="toggleFavourite">{{likeBtnContent}}</button>
            </div>
        </div>
    `,
    props: {
        film: Object
    },
    data: function () {
        return {
            poster: 'placeholder.png',
            initialPoster: '',
            imdbID: this.film.imdbID,
            favourite: this.film.favourite || false,
            plot: ''
        }
    },
    computed: {
        title: function () {
            return (this.film.Title !== 'N/A') ? this.film.Title : 'No title'
        },
        year: function () {
            return (this.film.Year !== 'N/A') ? this.film.Year.match(/\d{4}/)[0] : 'No year'
        },
        likeBtnContent: function () {
            return this.favourite ? 'Dislike' : 'Like'
        }
    },
    methods: {
        toggleFavourite: function () {
            let existing = JSON.parse(localStorage.getItem(KEY) || '[]')
            let index = existing.findIndex(e => {
                return e.imdbID === this.imdbID
            })
            if (this.favourite) {
                this.favourite = false
                if (index === -1) return
                existing = existing.filter(e => e.imdbID !== this.imdbID)
                if (!existing.length) {
                    localStorage.removeItem(KEY)
                    return
                }
            } else {
                this.favourite = true
                if (index !== -1) return
                existing.push ({
                    imdbID: this.imdbID,
                    Poster: this.initialPoster,
                    Title: this.title,
                    Year: this.year,
                    favourite: true
                })
            }
            localStorage.setItem(KEY, JSON.stringify(existing)) 
        },
        open: async function () {
            const queryParams = {
                i: this.imdbID,
                apikey: 'd5677312'
            }
            const query = form.encodeParams(queryParams)
            const requestURL = `${BASE_URL}/?${query}`
    
            try {
                let data = await form.request(requestURL)
                this.$router.push('/film', data)
            } catch(err) {
                console.error(err);
                
                // if (err.Error) this.error(err.Error)
                // else {
                //     this.error('Sorry, something wents wrong.')
                //     new Error(err)
                // }
            }
        },
        requestPoster: function (url) {
            const image = new Image();
            let poster = new Promise(function (resolve, reject) {
                image.onload = () => resolve(image.src)
                image.onerror = reject
            })
            image.src = url;
            return poster
        },
        getPosterUrl: async function () {
            this.initialPoster = this.film.Poster
            try {
                let poster = await this.requestPoster((this.film.Poster !== 'N/A') ? this.film.Poster : 'placeholder.png')
                this.poster = poster
            } catch (err) {
                this.poster = 'placeholder.png'
            }
        }
    },
    watch: {
        film: function () {
            this.getPosterUrl()
        }
    },
    created: function () {
        this.getPosterUrl()
    }
})

// view 1
const HomeComponent = {
    template: `
        <div class="search-wr">
            <div class="active-elemets">
                <div class="container">
                    <h1>Search for film</h1>
                    <search-component
                        @search="setSearchedFilms">
                    </search-component>
                    <results-component
                        :error-status="errorStatus"
                        :films="filmsToView">
                    </results-component>
                </div>
            </div>
        </div>
    `
}

// view 2
const FilmInfo = {
    template: `
        <div class="current-film__wr hidden">
            <div class="container">
                <div class="film-title">
                    <button class="back">Back</button>
                    <h2 class="name"></h2>
                    <button class="favourite">Like</button>
                </div>

                <div class="film-info">
                    <div class="poster">
                        <img src="" alt="Картинка">
                    </div>
                    <div class="film-descr">
                        <p class="year"></p>
                        <p class="description"></p>
                    </div>
                </div>                    
            </div>
        </div>
    `,
}

const router = new VueRouter({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: HomeComponent
        },
        {
            path: '/film_info',
            name: 'filmInfo',
            component: FilmInfo
        }
    ]
})

const vm = new Vue({
    el: '#app',
    router,
    components: {
        'home-component': HomeComponent,
        'film-info': FilmInfo
    },
    data: {
        errorStatus: {
            is: false,
            message: 'Sorry, something went wrong.'
        },
        filmsToView: []
    },
    methods: {
        setSearchedFilms: function (films) {
            this.filmsToView = films
        }
    }
})
